Elementary OS Steam Skin
========================
A skin for [Steam](http://store.steampowered.com/), designed to make Steam integrate more smoothly in [elementary OS](http://elementary.io/).

![Elementary Steam Skin][screenshot]

Install instructions
--------------------
* Unpack the `elementary` folder into `~/.steam/skins`.
* Launch Steam and navigate to `Steam > Settings`.
* Click the Interface tab. Change `< default skin>` to `elementary` and click OK.
* Now Steam will restart and the skin will be applied.

[screenshot]: https://raw.github.com/alexgleason/elementary-steam-skin/master/screenshot.png "Elementary Steam Skin"
